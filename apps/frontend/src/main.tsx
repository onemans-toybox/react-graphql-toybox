import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
} from "@apollo/client";
import { RetryLink } from "@apollo/client/link/retry";

import "./index.css";

const retryLink = new RetryLink({
  attempts: {
    max: 3,
  },
  delay: {
    initial: 3000,
    max: 3000,
  },
});

const v1HttpLink = new HttpLink({
  uri: "/v1/graphql",
});

const v2HttpLink = new HttpLink({
  uri: "/v2/graphql",
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: retryLink.split(
    (operation) => {
      console.log(operation.getContext());
      return operation.getContext()["clientName"] === "v2";
    },
    v2HttpLink,
    v1HttpLink
  ),
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>
);
