import express from "express";
import http from "http";
import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { json } from "body-parser";

(async () => {
  const app = express();
  const httpServer = http.createServer(app);

  const v1Server = new ApolloServer({
    typeDefs: `#graphql
    type Book {
      title: String
      author: String
    }
  
    type Query {
      books: [Book]
    }
  `,
    resolvers: {
      Query: {
        books: () => [
          {
            title: "The Awakening",
            author: "Kate Chopin",
          },
          {
            title: "City of Glass",
            author: "Paul Auster",
          },
        ],
      },
    },
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });
  const v2Server = new ApolloServer({
    typeDefs: `#graphql
    type Movie {
      title: String
      director: String
    }
  
    type Query {
      movies: [Movie]
    }
  `,
    resolvers: {
      Query: {
        movies: () => [
          {
            title: "Star Wars",
            director: "George Walton Lucas, Jr.",
          },
          {
            title: "Toy Story",
            director: "John Alan Lasseterr",
          },
        ],
      },
    },
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });

  await v1Server.start();
  await v2Server.start();

  app.use(`/v1/graphql`, json(), expressMiddleware(v1Server));
  app.use(`/v2/graphql`, json(), expressMiddleware(v2Server));

  await new Promise<void>((resolve) =>
    httpServer.listen({ port: 4000 }, resolve)
  );
  console.log(`🚀 Server ready at http://localhost:4000/v1/graphql`);
  console.log(`🚀 Server ready at http://localhost:4000/v2/graphql`);
})();
